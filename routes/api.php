<?php

use App\Http\Controllers\Equipment\EquipmentController;
use App\Http\Controllers\Fabric\FabricController;
use App\Http\Controllers\Product\ProductController;
use App\Http\Controllers\Types\TypeController;
use App\Http\Controllers\User\AuthController;
use App\Http\Controllers\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('/auth')->group(function () {
    Route::post('/registration', [AuthController::class, 'registration']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/logout', [AuthController::class, 'logout']);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('/user')->group(function () {
        Route::get('/me', [UserController::class, 'me']);
        Route::post('/update/{id}', [UserController::class, 'update']);
        Route::post('/delete/{id}', [UserController::class, 'delete']);
    });

    Route::prefix('/types')->group(function () {
        Route::get('/list', [TypeController::class, 'list']);
        Route::get('/byId/{id}', [TypeController::class, 'byId']);
        Route::post('/create', [TypeController::class, 'create']);
        Route::post('/update/{id}', [TypeController::class, 'update']);
        Route::post('/delete/{id}', [TypeController::class, 'delete']);
    });

    Route::prefix('/equipment')->group(function () {
        Route::get('/list', [EquipmentController::class, 'list']);
        Route::get('/byId/{id}', [EquipmentController::class, 'byId']);
        Route::post('/create', [EquipmentController::class, 'create']);
        Route::post('/update/{id}', [EquipmentController::class, 'update']);
        Route::post('/delete/{id}', [EquipmentController::class, 'delete']);
        Route::get('/check', [EquipmentController::class, 'check']);
    });

    Route::prefix('/products')->group(function () {
        Route::get('/list', [ProductController::class, 'list']);
        Route::get('/byId/{id}', [ProductController::class, 'byId']);
        Route::post('/create', [ProductController::class, 'create']);
        Route::post('/update/{id}', [ProductController::class, 'update']);
        Route::post('/delete/{id}', [ProductController::class, 'delete']);
    });

    Route::prefix('/fabric')->group(function () {
        Route::get('/list', [FabricController::class, 'list']);
        Route::get('/byId/{id}', [FabricController::class, 'byId']);
        Route::post('/create', [FabricController::class, 'create']);
        Route::post('/update/{id}', [FabricController::class, 'update']);
        Route::post('/delete/{id}', [FabricController::class, 'delete']);
    });
});
