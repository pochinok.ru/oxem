<?php

namespace App\Providers;

use App\Http\Service\Equipment\EquipmentService;
use App\Http\Service\Fabric\FabricService;
use App\Http\Service\Product\ProductService;
use App\Http\Service\Type\TypeService;
use App\Models\Interfaces\Equipment\EquipmentInterface;
use App\Models\Interfaces\Fabric\FabricInterface;
use App\Models\Interfaces\Product\ProductInterface;
use App\Models\Interfaces\Type\TypeInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(EquipmentInterface::class, EquipmentService::class);
        $this->app->bind(TypeInterface::class, TypeService::class);
        $this->app->bind(ProductInterface::class, ProductService::class);
        $this->app->bind(FabricInterface::class, FabricService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
