<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\Notifiable;
/**
 * Модель для хранения данных о продуктах.
 *
 * @property int $id Идентификатор записи в базе данных.
 * @property string $name Название продукта.
 * @property int $equipment_id Идентификатор оборудования.
 * @property float $price Цена продукта за единицу.
 * @property int $output Количество продуктов в час.
 * @property string $expiration_date Срок годности продукта в месяцах.
 * @property string $created_at Дата и время создания записи.
 *
 * @package App\Models
 */
class Product extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'equipment_id',
        'price',
        'expiration_date',
        'created_at',
    ];

    public function equipment(): BelongsTo
    {
        return $this->belongsTo(Equipment::class,'equipment_id');
    }

    public function fabric(): BelongsTo
    {
        return $this->belongsTo(Fabric::class);
    }
}
