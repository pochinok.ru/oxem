<?php

namespace App\Models\Sorts\Equipment;


class EquipmentSort
{
    /**
     * @var string Сначала максимальная цена.
     */
    const MAX_PRICE = 'maxPrice';

    /**
     * @var string Сначала минимальная цена.
     */
    const MIN_PRICE = 'minPrice';

    /**
     * @var string Сначала новые.
     */
    const NEW = 'new';

    /**
     * @var string Сначала старые.
     */
    const OLD = 'old';

    /**
     * Возвращает допустимые значения.
     *
     * @return string[]
     */
    public static function getValues(): array
    {
        return [
            self::NEW,
            self::OLD,
            self::MAX_PRICE,
            self::MIN_PRICE,
        ];
    }
}
