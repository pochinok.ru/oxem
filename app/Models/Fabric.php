<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
/**
 * Модель для хранения данных о процессах работы фабрики.
 *
 * @property int $id Идентификатор записи в базе данных.
 * @property int $product_id Идентификатор продукта.
 * @property string $name Название процесса работы.
 * @property int $hours Количество отработанных часов.
 * @property int $product_count Количество произведенных продуктов.
 * @property float $total_price Общая стоимость процесса работы.
 * @property string $created_at Дата и время создания записи.
 *
 * @package App\Models
 */
class Fabric extends Model
{
    use HasFactory, Notifiable;

    /**
     * Название таблицы, в которой хранятся данные о процессах работы фабрики.
     *
     * @var string
     */
    protected $table = 'fabric';

    /**
     * Атрибуты модели, которые могут быть массово присвоены.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'name',
        'hours',
        'product_count',
        'total_price',
        'created_at',
    ];

    public function product(): HasMany
    {
        return $this->hasMany(Product::class,'product_id');
    }

}
