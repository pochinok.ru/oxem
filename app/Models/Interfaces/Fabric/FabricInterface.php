<?php

namespace App\Models\Interfaces\Fabric;

use App\Http\Requests\Equipment\ProductUpdateRequest;
use App\Http\Requests\Fabric\FabricCreateRequest;
use App\Http\Requests\Fabric\FabricUpdateRequest;
use App\Http\Requests\Product\ProductCreateRequest;
use Illuminate\Http\JsonResponse;

interface FabricInterface
{
    /**
     * @param int $perPage
     * @param int $page
     * @param string|null $keyword
     * @param string|null $sort
     * @return JsonResponse
     */
    public function list(
        int $perPage,
        int $page,
        ?string $keyword,
        ?string $sort,
    ): JsonResponse;

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function byId(int $id): JsonResponse;

    /**
     * @param FabricCreateRequest $request
     * @return JsonResponse
     */
    public function create(FabricCreateRequest $request): JsonResponse;

    /**
     * @param int $id
     * @param FabricUpdateRequest $request
     * @return JsonResponse
     */
    public function update(int $id, FabricUpdateRequest $request): JsonResponse;
}
