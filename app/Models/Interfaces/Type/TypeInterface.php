<?php

namespace App\Models\Interfaces\Type;

use App\Http\Requests\Type\TypeCreateRequest;
use Illuminate\Http\JsonResponse;

interface TypeInterface
{
    /**
     * @param int $perPage
     * @param int $page
     * @param string|null $keyword
     * @param string|null $sort
     * @param string|null $type
     * @return JsonResponse
     */
    public function list(
        int $perPage,
        int $page,
    ): JsonResponse;

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function byId(int $id): JsonResponse;

    /**
     * @param TypeCreateRequest $request
     * @return JsonResponse
     */
    public function create(TypeCreateRequest $request): JsonResponse;

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse;
}
