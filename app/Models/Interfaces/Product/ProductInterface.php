<?php

namespace App\Models\Interfaces\Product;

use App\Http\Requests\Equipment\ProductUpdateRequest;
use App\Http\Requests\Product\ProductCreateRequest;
use Illuminate\Http\JsonResponse;

interface ProductInterface
{
    /**
     * @param int $perPage
     * @param int $page
     * @param string|null $keyword
     * @param string|null $sort
     * @return JsonResponse
     */
    public function list(
        int $perPage,
        int $page,
        ?string $keyword,
        ?string $sort,
    ): JsonResponse;

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function byId(int $id): JsonResponse;

    /**
     * @param ProductCreateRequest $request
     * @return JsonResponse
     */
    public function create(ProductCreateRequest $request): JsonResponse;

    /**
     * @param int $id
     * @param ProductUpdateRequest $request
     * @return JsonResponse
     */
    public function update(int $id, ProductUpdateRequest $request): JsonResponse;
}
