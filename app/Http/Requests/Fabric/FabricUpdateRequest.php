<?php

namespace App\Http\Requests\Fabric;

use Illuminate\Foundation\Http\FormRequest;

class FabricUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string',
            'product_id' => 'nullable|integer',
            'hours' => 'nullable|integer',
        ];
    }
}
