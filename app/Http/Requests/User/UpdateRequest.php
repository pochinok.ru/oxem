<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Класс валидации запроса для обновления данных пользователя.
     *
     * @OA\Schema(
     *     title="Запрос для обновления данных пользователя",
     *     type="object",
     *     required={"email", "phone", "name", "surname", "patronymic"},
     *     @OA\Property(
     *          property="email",
     *          title="Email",
     *          type="string",
     *          example="test1@gmail.com",
     *     ),
     *     @OA\Property(
     *          property="phone",
     *          title="Телефон",
     *          type="integer",
     *          example=89676634332,
     *          maxLength=12,
     *          minLength=10,
     *          nullable=true
     *     ),
     *     @OA\Property(
     *          property="name",
     *          title="Имя",
     *          type="string",
     *     ),
     *     @OA\Property(
     *          property="surname",
     *          title="Фамилия",
     *          type="string",
     *     ),
     *     @OA\Property(
     *          property="patronymic",
     *          title="Отчество",
     *          type="string",
     *     )
     * )
     *
     * @package App\Http\Requests\Users
     * @author Danya Pochinok
     */


    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'nullable|string|unique:users,email',
            'phone_number' => 'nullable|integer|unique:users,phone_number|min:10',
            'name' => 'nullable|string',
            'surname' => 'nullable|string',
            'patronymic' => 'nullable|string',
        ];
    }
}
