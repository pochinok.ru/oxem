<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Класс валидации запроса для авторизации пользователя.
 *
 * @OA\Schema(
 *     title="Запрос авторизации пользователя",
 *     type="object",
 *     required={"email", "password"},
 *     @OA\Property(
 *          property="email",
 *          title="Email",
 *          type="string",
 *          example="test@gmail.com",
 *     ),
 *     @OA\Property(
 *          property="password",
 *          title="Пароль",
 *          type="string",
 *          example="12345678",
 *          minLength=8
 *     )
 * )
 *
 * @package App\Http\Requests\Users
 * @author Danya Pochinok
 */
class LoginRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ];
    }
}
