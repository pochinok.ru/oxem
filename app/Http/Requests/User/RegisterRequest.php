<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Класс валидации запроса для регистрации пользователя.
     *
     * @OA\Schema(
     *     title="Запрос регистрации пользователя",
     *     type="object",
     *     required={"email", "password", "name"},
     *     @OA\Property(
     *          property="email",
     *          title="Email",
     *          type="string",
     *          example="test@gmail.com",
     *     ),
     *     @OA\Property(
     *          property="password",
     *          title="Пароль",
     *          type="string",
     *          example="12345678",
     *          minLength=8
     *     ),
     *     @OA\Property(
     *          property="name",
     *          title="Имя",
     *          type="string",
     *     ),
     * )
     *
     * @package App\Http\Requests\Users
     * @author Danya Pochinok
     */

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|string|unique:users,email',
            'password' => 'required|string|min:8',
            'name' => 'required|string',
        ];
    }
}
