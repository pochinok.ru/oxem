<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Service\UserService;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function me(): JsonResponse
    {
        return $this->userService->me();
    }

    public function update(int $id, UpdateRequest $request): JsonResponse
    {
        return response()->json($this->userService->update($id, $request));
    }

    public function delete(int $id): JsonResponse
    {
        return $this->userService->delete($id);
    }
}
