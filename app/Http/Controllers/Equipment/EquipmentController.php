<?php

namespace App\Http\Controllers\Equipment;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Fabric\FabricController;
use App\Http\Requests\Equipment\ProductUpdateRequest;
use App\Http\Requests\Fabric\FabricCreateRequest;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Http\Service\Equipment\EquipmentService;
use App\Models\Fabric;
use App\Models\Interfaces\Equipment\EquipmentInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    protected EquipmentInterface $equipmentService;

    public function __construct
    (
        EquipmentInterface $equipmentService,
    )
    {
        $this->equipmentService = $equipmentService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        return $this->equipmentService->list(
            $request->input('perPage', 18),
            $request->input('page', 1),
            $request->input('keyword'),
            $request->input('sort'),
            $request->input('type'),
        );
    }

    public function byId(int $id): JsonResponse
    {
        return $this->equipmentService->byId($id);
    }

    /**
     * @param ProductCreateRequest $request
     */
    public function create(ProductCreateRequest $request)
    {
       $this->equipmentService->create($request);
    }

    public function update(int $id, ProductUpdateRequest $request)
    {
       $this->equipmentService->update($id, $request);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        return $this->equipmentService->delete($id);
    }

    public function check() {
        //Создание оборудования разного типа
        for ($i = 0; $i <= 2; $i++) {
            app(EquipmentService::class)->create(new ProductCreateRequest([
                'name' => 'Машина для шоколада'.$i,
                'type' => 2,
                'output' => random_int(1, 9999),
            ]));
            $i++;
        }

        for ($i = 0; $i <= 3; $i++) {
            app(EquipmentService::class)->create(new ProductCreateRequest([
                'name' => 'Машина для мороженного'.$i,
                'type' => 1,
                'output' => random_int(1, 9999),
            ]));
            $i++;
        }

        app(EquipmentService::class)->create(new ProductCreateRequest([
            'name' => 'Машина для выпечки',
            'type' => 3,
            'output' => random_int(1, 9999),
        ]));

        //Вывод информации по количеству текущего оборудования
        $types = [2 => 'Шоколад', 1 => 'Мороженное', 3 => 'Выпечка'];
        foreach ($types as $type => $label) {
            $response = app(EquipmentController::class)->list(new Request(['type' => $type]));
            $data = $response->getData();
            $total = $data->total;
            echo 'Количество оборудования по производству ' . $label . ': ' . $total . PHP_EOL;
        }

        //Создание продукции согласно часам(12 часов)
        app(FabricController::class)->create(new FabricCreateRequest([
            'name' => 'Изготовление мороженного',
            'product_id' => 15,
            'hours' => 12
        ]));
        app(FabricController::class)->create(new FabricCreateRequest([
            'name' => 'Изготовление шоколада',
            'product_id' => 3,
            'hours' => 12
        ]));
        app(FabricController::class)->create(new FabricCreateRequest([
            'name' => 'Изготовление выпечки',
            'product_id' => 18,
            'hours' => 12
        ]));

        echo 'Список готовой продукции';
        echo "\n", "\n";

        $productIds = [18, 3, 15];
        foreach ($productIds as $productId) {
            $data = Fabric::where('product_id', $productId)->get();
            $totalProductCount = $data->sum('product_count');
            $totalPrice = $data->sum('total_price');
            $name = $data->first()->name;
            echo "Наименование: " . $name . "\n";
            echo "Сумма всей готовой продукции: " . $totalProductCount . "\n";
            echo "Сумма общей стоимости продукции: " . $totalPrice . "\n";
            echo "\n";
        }

        app(EquipmentService::class)->create(new ProductCreateRequest([
            'name' => 'Машина для мороженного',
            'type' => 1,
            'output' => random_int(1, 9999),
        ]));

        app(EquipmentService::class)->create(new ProductCreateRequest([
            'name' => 'Машина для выпечки',
            'type' => 3,
            'output' => random_int(1, 9999),
        ]));

        //Вывод информации по количеству текущего оборудования
        $types = [2 => 'Шоколад', 1 => 'Мороженное', 3 => 'Выпечка'];
        foreach ($types as $type => $label) {
            $response = app(EquipmentController::class)->list(new Request(['type' => $type]));
            $data = $response->getData();
            $total = $data->total;
            echo 'Количество оборудования по производству ' . $label . ': ' . $total . PHP_EOL;
        }

        //Создание продукции согласно часам(12 часов)
        app(FabricController::class)->create(new FabricCreateRequest([
            'name' => 'Изготовление мороженного',
            'product_id' => 15,
            'hours' => 12
        ]));
        app(FabricController::class)->create(new FabricCreateRequest([
            'name' => 'Изготовление шоколада',
            'product_id' => 3,
            'hours' => 12
        ]));
        app(FabricController::class)->create(new FabricCreateRequest([
            'name' => 'Изготовление выпечки',
            'product_id' => 18,
            'hours' => 12
        ]));

        echo 'Список готовой продукции';
        echo "\n", "\n";

        $productIds = [18, 3, 15];
        foreach ($productIds as $productId) {
            $data = Fabric::where('product_id', $productId)->get();
            $totalProductCount = $data->sum('product_count');
            $totalPrice = $data->sum('total_price');
            $name = $data->first()->name;
            echo "Наименование: " . $name . "\n";
            echo "Сумма всей готовой продукции: " . $totalProductCount . "\n";
            echo "Сумма общей стоимости продукции: " . $totalPrice . "\n";
            echo "\n";
        }
    }
}
