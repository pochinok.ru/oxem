<?php

namespace App\Http\Controllers\Fabric;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipment\ProductUpdateRequest;
use App\Http\Requests\Fabric\FabricCreateRequest;
use App\Http\Requests\Fabric\FabricUpdateRequest;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Models\Interfaces\Fabric\FabricInterface;
use App\Models\Interfaces\Product\ProductInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FabricController extends Controller
{
    protected FabricInterface $fabric;

    public function __construct
    (
        FabricInterface $fabric,
    )
    {
        $this->fabric = $fabric;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        return $this->fabric->list(
            $request->input('perPage', 18),
            $request->input('page', 1),
            $request->input('keyword'),
            $request->input('sort'),
        );
    }

    public function byId(int $id): JsonResponse
    {
        return $this->fabric->byId($id);
    }

    /**
     * @param FabricCreateRequest $request
     */
    public function create(FabricCreateRequest $request)
    {
       $this->fabric->create($request);
    }

    public function update(int $id, FabricUpdateRequest $request)
    {
       $this->fabric->update($id, $request);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        return $this->fabric->delete($id);
    }
}
