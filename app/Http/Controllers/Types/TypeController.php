<?php

namespace App\Http\Controllers\Types;

use App\Http\Controllers\Controller;
use App\Http\Requests\Type\TypeCreateRequest;
use App\Models\Interfaces\Type\TypeInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    protected TypeInterface $typeInterface;

    public function __construct
    (
        TypeInterface $typeInterface,
    )
    {
        $this->typeInterface = $typeInterface;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        return $this->typeInterface->list(
            $request->input('perPage', 18),
            $request->input('page', 1),
        );
    }

    public function byId(int $id): JsonResponse
    {
        return $this->typeInterface->byId($id);
    }

    /**
     * @param TypeCreateRequest $request
     */
    public function create(TypeCreateRequest $request)
    {
       $this->typeInterface->create($request);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        return $this->typeInterface->delete($id);
    }
}
