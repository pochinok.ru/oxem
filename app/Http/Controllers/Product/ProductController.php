<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipment\ProductUpdateRequest;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Models\Interfaces\Product\ProductInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected ProductInterface $productInterface;

    public function __construct
    (
        ProductInterface $productInterface,
    )
    {
        $this->productInterface = $productInterface;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request): JsonResponse
    {
        return $this->productInterface->list(
            $request->input('perPage', 18),
            $request->input('page', 1),
            $request->input('keyword'),
            $request->input('sort'),
        );
    }

    public function byId(int $id): JsonResponse
    {
        return $this->productInterface->byId($id);
    }

    /**
     * @param ProductCreateRequest $request
     */
    public function create(ProductCreateRequest $request)
    {
       $this->productInterface->create($request);
    }

    public function update(int $id, ProductUpdateRequest $request)
    {
       $this->productInterface->update($id, $request);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        return $this->productInterface->delete($id);
    }
}
