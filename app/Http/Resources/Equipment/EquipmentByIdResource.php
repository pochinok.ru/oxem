<?php

namespace App\Http\Resources\Equipment;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;

class EquipmentByIdResource extends JsonResource
{
    /**
     * @param Request|null $request
     * @return array
     */
    public function toArray(Request $request = null): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type->name,
            'output' => $this->output,
            'createdAt' => $this->created_at,
            'uniqueName' => $this->unique_name,
        ];
    }
}


