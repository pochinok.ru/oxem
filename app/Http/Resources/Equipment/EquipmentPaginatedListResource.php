<?php

namespace App\Http\Resources\Equipment;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;

class EquipmentPaginatedListResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request = null): array
    {
        $items = [];
        /** @var LengthAwarePaginator $paginator */
        $paginator = $this->getEquipment();

        foreach ($paginator->items() as $equipment) {

            $items[] = [
                'id' => $equipment->id,
                'name' => $equipment->name,
                'type' => $equipment->type->name,
                'output' => $equipment->output,
                'createdAt' => $equipment->created_at,
                'uniqueName' => $equipment->unique_name,
            ];
        }

        return (
        new LengthAwarePaginator(
            $items,
            $paginator->total(),
            $paginator->perPage(),
            $paginator->currentPage()
        )
        )
            ->toArray();
    }

    public function getEquipment()
    {
        return $this->resource;
    }
}

