<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductByIdResource extends JsonResource
{
    /**
     * @param Request|null $request
     * @return array
     */
    public function toArray(Request $request = null): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'createdAt' => $this->created_at,
        ];
    }
}


