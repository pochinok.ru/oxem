<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductPaginatedListResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request = null): array
    {
        $items = [];
        /** @var LengthAwarePaginator $paginator */
        $paginator = $this->getProduct();

        foreach ($paginator->items() as $equipment) {

            $items[] = [
                'id' => $equipment->id,
                'name' => $equipment->name,
                'createdAt' => $equipment->created_at,
            ];
        }

        return (
        new LengthAwarePaginator(
            $items,
            $paginator->total(),
            $paginator->perPage(),
            $paginator->currentPage()
        )
        )
            ->toArray();
    }

    public function getProduct()
    {
        return $this->resource;
    }
}

