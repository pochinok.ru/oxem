<?php

namespace App\Http\Resources\Fabric;

use App\Http\Resources\Product\ProductByIdResource;
use App\Http\Resources\User\UserResource;
use App\Http\Service\Product\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;

class FabricPaginatedListResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request = null): array
    {
        $items = [];
        /** @var LengthAwarePaginator $paginator */
        $paginator = $this->getFabric();

        foreach ($paginator->items() as $fabric) {

            $items[] = [
                'id' => $fabric->id,
                'product' => app(ProductService::class)->byId($fabric->product_id),
                'name' => $fabric->name,
                'hours' => $fabric->hours,
                'productCount' => $fabric->product_count,
                'totalPrice' => $fabric->total_price,
                'createdAt' => $fabric->created_at,
            ];
        }

        return (
        new LengthAwarePaginator(
            $items,
            $paginator->total(),
            $paginator->perPage(),
            $paginator->currentPage()
        )
        )
            ->toArray();
    }

    public function getFabric()
    {
        return $this->resource;
    }
}

