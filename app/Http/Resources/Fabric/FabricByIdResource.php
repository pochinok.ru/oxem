<?php

namespace App\Http\Resources\Fabric;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;

class FabricByIdResource extends JsonResource
{
    /**
     * @param Request|null $request
     * @return array
     */
    public function toArray(Request $request = null): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'product' => $this->product_id->name,
            'hours' => $this->hours,
            'productCount' => $this->product_count,
            'totalPrice' => $this->total_price,
            'createdAt' => $this->created_at,
        ];
    }
}


