<?php

namespace App\Http\Service\Equipment;

use App\Http\Repository\Equipment\EquipmentRepository;
use App\Http\Requests\Equipment\ProductUpdateRequest;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Http\Resources\Equipment\EquipmentByIdResource;
use App\Http\Resources\Equipment\EquipmentPaginatedListResource;
use App\Models\Equipment;
use App\Models\Interfaces\Equipment\EquipmentInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class EquipmentService implements EquipmentInterface
{
    protected EquipmentRepository $equipmentRepository;

    public function __construct(
        EquipmentRepository $equipmentRepository,
    )
    {
        $this->equipmentRepository = $equipmentRepository;
    }

    public function list(
        $perPage,
        $page,
        ?string $keyword,
        ?string $sort,
        ?int $type
    ): JsonResponse
    {
        return response()->json(new EquipmentPaginatedListResource($this->equipmentRepository->paginatedList(
            $perPage,
            $page,
            $keyword,
            $sort,
            $type
        )));
    }

    public function byId(int $id): JsonResponse
    {
        return response()->json(new EquipmentByIdResource($this->equipmentRepository->find($id)));
    }

    public function create(ProductCreateRequest $request): JsonResponse
    {
        Equipment::create([
           'name'=> $request->name,
           'type_id'=> $request->type,
           'output'=> $request->output,
           'unique_name'=> uniqid(),
        ]);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    public function update(int $id, ProductUpdateRequest $request): JsonResponse
    {
        $this->equipmentRepository->update($id, $request->validated());

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    public function delete($id): JsonResponse
    {
        $this->equipmentRepository->find($id)->delete();

        return response()->json(['Запись удалена'], 200);
    }
}
