<?php

namespace App\Http\Service\Product;

use App\Http\Repository\Product\ProductRepository;
use App\Http\Requests\Equipment\ProductUpdateRequest;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Http\Resources\Product\ProductByIdResource;
use App\Http\Resources\Product\ProductPaginatedListResource;
use App\Models\Interfaces\Product\ProductInterface;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ProductService implements ProductInterface
{
    protected ProductRepository $productRepository;

    public function __construct(
        ProductRepository $productRepository,
    )
    {
        $this->productRepository = $productRepository;
    }

    public function list(
        $perPage,
        $page,
        ?string $keyword,
        ?string $sort,
    ): JsonResponse
    {
        return response()->json(new ProductPaginatedListResource($this->productRepository->paginatedList(
            $perPage,
            $page,
            $keyword,
            $sort,
        )));
    }

    public function byId(int $id): JsonResponse
    {
        return response()->json(new ProductByIdResource($this->productRepository->find($id)));
    }

    public function create(ProductCreateRequest $request): JsonResponse
    {
        Product::create([
           'name'=> $request->name,
           'equipment_id'=> $request->equipment_id,
           'price'=> $request->price,
           'expiration_date'=> $request->expiration_date,
        ]);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    public function update(int $id, ProductUpdateRequest $request): JsonResponse
    {
        $this->productRepository->update($id, $request->validated());

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    public function delete($id): JsonResponse
    {
       $this->productRepository->find($id)->delete();

       return response()->json(['Запись удалена'], 200);
    }
}
