<?php

namespace App\Http\Service\Type;

use App\Http\Repository\Type\TypeRepository;
use App\Http\Requests\Type\TypeCreateRequest;
use App\Http\Resources\Product\ProductByIdResource;
use App\Http\Resources\Type\TypePaginatedListResource;
use App\Models\Interfaces\Type\TypeInterface;
use App\Models\Type;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class TypeService implements TypeInterface
{
    protected TypeRepository $typeRepository;

    public function __construct(
        TypeRepository $typeRepository,
    )
    {
        $this->typeRepository = $typeRepository;
    }

    public function list(
        $perPage,
        $page,
    ): JsonResponse
    {
        return response()->json(new TypePaginatedListResource($this->typeRepository->paginatedList(
            $perPage,
            $page,
        )));
    }

    public function byId(int $id): JsonResponse
    {
        return response()->json(new ProductByIdResource($this->typeRepository->find($id)));
    }

    public function create(TypeCreateRequest $request): JsonResponse
    {
        Type::create([
           'name'=> $request->name,
           'type'=> $request->type,
           'output'=> $request->output,
           'unique_name'=> uniqid(),
        ]);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    public function delete($id): JsonResponse
    {
        $this->typeRepository->find($id)->delete();

        return response()->json(['Запись удалена'], 200);
    }
}
