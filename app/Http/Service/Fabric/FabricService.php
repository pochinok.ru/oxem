<?php

namespace App\Http\Service\Fabric;

use App\Http\Controllers\Equipment\EquipmentController;
use App\Http\Repository\Fabric\FabricRepository;
use App\Http\Requests\Fabric\FabricCreateRequest;
use App\Http\Requests\Fabric\FabricUpdateRequest;
use App\Http\Resources\Fabric\FabricPaginatedListResource;
use App\Http\Resources\Product\ProductByIdResource;
use App\Http\Service\Product\ProductService;
use App\Models\Equipment;
use App\Models\Fabric;
use App\Models\Interfaces\Fabric\FabricInterface;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FabricService implements FabricInterface
{
    protected FabricRepository $fabricRepository;

    public function __construct(
        FabricRepository $fabricRepository,
    )
    {
        $this->fabricRepository = $fabricRepository;
    }

    public function list(
        $perPage,
        $page,
        ?string $keyword,
        ?string $sort,
    ): JsonResponse
    {
        return response()->json(new FabricPaginatedListResource($this->fabricRepository->paginatedList(
            $perPage,
            $page,
        )));
    }

    public function byId(int $id): JsonResponse
    {
        return response()->json(new ProductByIdResource($this->fabricRepository->find($id)));
    }

    public function create(FabricCreateRequest $request): JsonResponse
    {
        //Считает общее количество произведенной продукции за n часов.
        $productCount = Equipment::query()
            ->where('id', function ($query) use ($request) {
                $query->select('equipment_id')
                    ->from('products')
                    ->where('id', $request->product_id);
            })
            ->value('output')
            * $request->hours;

        //Считает полную стоимость всей продукции за n количество продуктов
        $price = Product::query()->where('id', $request->product_id)->value('price') * $productCount;

        Fabric::create([
           'name'=> $request->name,
           'product_id'=> $request->product_id,
           'hours'=> $request->hours,
           'product_count'=> $productCount,
           'total_price'=> $price,
        ]);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    public function update(int $id, FabricUpdateRequest $request): JsonResponse
    {
        $fabric = $this->fabricRepository->find($id);

        $fabric->update($request->validated());

        if ($fabric->product_id && $fabric->hours) {
            $productCount = Equipment::query()
                    ->where('id', function ($query) use ($fabric) {
                        $query->select('equipment_id')
                            ->from('products')
                            ->where('id', $fabric->product_id);
                    })
                    ->value('output')
                * $fabric->hours;

            $price = Product::query()->where('id', $fabric->product_id)->value('price') * $productCount;
            $fabric->update([
                'product_count' => $productCount,
                'total_price' => $price,
            ]);
        }

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    public function delete($id): JsonResponse
    {
       $this->fabricRepository->find($id)->delete();

       return response()->json(['Запись удалена'], 200);
    }
}
