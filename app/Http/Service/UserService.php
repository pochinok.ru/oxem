<?php

namespace App\Http\Service;

use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Repository\User\UserRepository;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected UserRepository $userRepository;

    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param LoginRequest $request
     * @return array|string
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $credentials = $request->validated();
        if (Auth::attempt($credentials)) {
            return response()->json([
                'token' => auth()->user()->createToken('login')->plainTextToken
            ], Response::HTTP_OK);
        } else {
            return response()->json('Не авторизован', Response::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function registration(RegisterRequest $request): JsonResponse
    {
        User::create([
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'name' => $request->input('name'),
        ]);

        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    public function me()
    {
        try {
            return response()->json((new UserResource($this->userRepository->find(Auth::id())))->toArray());
        } catch (\Exception $e) {
            return response()->json(['message' => 'Не удалось выполнить запрос'], 500);
        }
    }

    public function update($id, UpdateRequest $request): JsonResponse
    {
        $this->userRepository->update($id, $request->validated());

        return response()->json(['successfully'], Response::HTTP_OK);
    }

    public function delete($id): JsonResponse
    {
        $authenticatedUserId = auth()->id();

        if ($id == $authenticatedUserId) {
            $user = $this->userRepository->find($id);
            $user->delete();

            return response()->json([], 200);
        } else {
            return response()->json('У вас нет прав для удаления этого профиля', Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * @return bool
     */
    public function logout(): bool
    {
        Auth::logout();
        return true;
    }
}
