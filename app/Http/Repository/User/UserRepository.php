<?php

namespace App\Http\Repository\User;
use App\Models\User;
use App\Http\Repository\Repository;
use Illuminate\Database\Eloquent\Collection;

class UserRepository extends Repository

{
    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function all(): Collection
    {
        return $this->model->all();
    }



}
