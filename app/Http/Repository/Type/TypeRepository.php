<?php

namespace App\Http\Repository\Type;
use App\Http\Repository\Repository;
use App\Models\Type;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class TypeRepository extends Repository
{
    /**
     * ProductRepository constructor.
     *
     * @param Type $model
     */
    public function __construct(Type $model)
    {
        $this->model = $model;
    }

    public function all(): Collection
    {
        return $this->model->all();
    }

    public function paginatedList(
        int $perPage,
        int $page,
    ): LengthAwarePaginator
    {
        $query = Type::query()->orderBy('created_at', 'desc');
        $offset = ($page - 1) * $perPage;
        $results = $query->offset($offset)->limit($perPage)->get();

        return new LengthAwarePaginator($results, $query->count(), $perPage, $page);
    }
}
