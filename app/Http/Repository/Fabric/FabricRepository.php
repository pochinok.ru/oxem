<?php

namespace App\Http\Repository\Fabric;
use App\Http\Repository\Repository;
use App\Models\Fabric;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class FabricRepository extends Repository
{
    /**
     *
     * @param Fabric $model
     */
    public function __construct(Fabric $model)
    {
        $this->model = $model;
    }

    public function all(): Collection
    {
        return $this->model->all();
    }

    public function paginatedList(
        int $perPage,
        int $page,
    ): LengthAwarePaginator
    {
        $query = Fabric::query()->orderBy('created_at', 'desc');
        $offset = ($page - 1) * $perPage;
        $results = $query->offset($offset)->limit($perPage)->get();

        return new LengthAwarePaginator($results, $query->count(), $perPage, $page);
    }
}
