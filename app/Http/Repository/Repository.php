<?php

namespace App\Http\Repository;

use Illuminate\Database\Eloquent\Model;

class Repository
{
    protected Model $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $create
     * @return Model
     */

    public function create(array $create): Model
    {
        return $this->model->create($create);
    }

    public function update($id, array $update)
    {
        $model = $this->model->find($id);
        return $model->update($update);
    }

    public function destroy($id): int
    {
        return $this->model->destroy($id);
    }

    public function find(int $id)
    {
        return $this->model->find($id);
    }
}

