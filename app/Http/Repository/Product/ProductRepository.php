<?php

namespace App\Http\Repository\Product;
use App\Http\Repository\Repository;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductRepository extends Repository
{
    /**
     * ProductRepository constructor.
     *
     * @param Product $model
     */
    public function __construct(
        Product $model,
    )
    {
        $this->model = $model;
    }

    public function all(): Collection
    {
        return $this->model->all();
    }

    public function paginatedList(
        int $perPage,
        int $page,
        ?string $keyword,
        ?string $sort,
    ): LengthAwarePaginator
    {
        $query = Product::query();

        if ($keyword) {
            $query->keywordSearch($keyword);
        }


        if ($sort) {
            $query->sort($sort);
        } else {
            $query->orderBy('created_at', 'desc');
        }

        $offset = ($page - 1) * $perPage;
        $results = $query->offset($offset)->limit($perPage)->get();

        return new LengthAwarePaginator($results, $query->count(), $perPage, $page);
    }
}
