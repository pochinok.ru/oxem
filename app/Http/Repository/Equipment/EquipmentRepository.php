<?php

namespace App\Http\Repository\Equipment;
use App\Models\Equipment;
use App\Http\Repository\Repository;
use App\Models\Sorts\Equipment\EquipmentSort;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class EquipmentRepository extends Repository
{
    /**
     *
     * @param Equipment $model
     */
    public function __construct(
        Equipment $model,
    )
    {
        $this->model = $model;
    }

    public function all(): Collection
    {
        return $this->model->all();
    }

    public function paginatedList(
        int $perPage,
        int $page,
        ?string $keyword,
        ?string $sort,
        ?int $type
    ): LengthAwarePaginator
    {
        $query = Equipment::query();

        if ($keyword) {
            $query->where(function ($q) use ($keyword) {
                $q->where('name', 'like', '%' . $keyword . '%')
                    ->orWhere('unique_name', 'like', '%' . $keyword . '%');
            });
        }

        if ($type) {
             $query->where(function ($q) use ($type) {
                $q->where('type_id', $type);
            });;
        }

        if ($sort) {
            switch ($sort) {
                case EquipmentSort::MIN_PRICE:
                    $query->orderBy('price',  'ASC');
                    break;
                case EquipmentSort::MAX_PRICE:
                    $query->orderBy('price',  'DESC');
                    break;
                case EquipmentSort::NEW:
                    $query->orderBy('created_at', 'DESC');
                    break;
                case EquipmentSort::OLD:
                    $query->orderBy('created_at', 'ASC');
                    break;
            }
        } else {
            $query->orderBy('created_at', 'desc');
        }

        $offset = ($page - 1) * $perPage;
        $results = $query->offset($offset)->limit($perPage)->get();

        return new LengthAwarePaginator($results, $query->count(), $perPage, $page);
    }
}
