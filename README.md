# oxem

Тестовое задание выполнил как Сервис, получилось более масштабно, чем планировалось по оценке времени и задумке ТЗ.
Но тут я показал как работаю с Docker, docker-compose, ООП и как выглядит мой код хоть и в тривиальных методах. 
Сделал всё с использованием БД, т.к посчитал необходимым реализацию отношений моделей, проще говоря, показать, как это всё работает 
в целом. 
Возможно это было не совсем верно и правильно, что я отошёл от прямого ТЗ, но Сервисная сторона должна выглядеть удобней.

Запуск окружения:

   ```
   git clone git@gitlab.com:pochinok.ru/oxem.git
   ```

 `.env` file:
   ```
   APP_NAME=oxem
   APP_ENV=local
   APP_KEY=base64:XCejoXnBO4B7mN/uO4u1W9RMVAWGPy8l9zD4jSONRm4=
   APP_DEBUG=true
   APP_URL=http://127.0.0.1
   LOG_CHANNEL=stack
   LOG_DEPRECATIONS_CHANNEL=null
   LOG_LEVEL=debug
   DB_CONNECTION=pgsql
   DB_HOST=postgres
   DB_PORT=5432
   DB_DATABASE=oxem_app
   DB_USERNAME=postgres
   DB_PASSWORD=root
   POSTGRES_PASSWORD=root
   BROADCAST_DRIVER=log
   CACHE_DRIVER=file
   FILESYSTEM_DISK=local
   QUEUE_CONNECTION=sync
   SESSION_DRIVER=file
   SESSION_LIFETIME=120
   MEMCACHED_HOST=127.0.0.1
   REDIS_HOST=127.0.0.1
   REDIS_PASSWORD=null
   REDIS_PORT=6379
   MAIL_MAILER=smtp
   MAIL_HOST=mailpit
   MAIL_PORT=1025
   MAIL_USERNAME=null
   MAIL_PASSWORD=null
   MAIL_ENCRYPTION=null
   MAIL_FROM_ADDRESS="hello@example.com"
   MAIL_FROM_NAME="${APP_NAME}"
   AWS_ACCESS_KEY_ID=
   AWS_SECRET_ACCESS_KEY=
   AWS_DEFAULT_REGION=us-east-1
   AWS_BUCKET=
   AWS_USE_PATH_STYLE_ENDPOINT=false
   PUSHER_APP_ID=
   PUSHER_APP_KEY=
   PUSHER_APP_SECRET=
   PUSHER_HOST=
   PUSHER_PORT=443
   PUSHER_SCHEME=https
   PUSHER_APP_CLUSTER=mt1
   VITE_APP_NAME="${APP_NAME}"
   VITE_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
   VITE_PUSHER_HOST="${PUSHER_HOST}"
   VITE_PUSHER_PORT="${PUSHER_PORT}"
   VITE_PUSHER_SCHEME="${PUSHER_SCHEME}"
   VITE_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

   ```

   ```
   docker network create oxem
   ```

   ```
   docker-compose build
   ```

   ```
   docker-compose up -d
   ```

## Database File

The `oxem_app.sql` file.

## API Requests

API requests:

 GET /equipment/check

 Отобразит в Postman результат по ТЗ

```
Остальные запросы выполняют стандартные функции CRUD
```

