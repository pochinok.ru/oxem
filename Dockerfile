FROM php:8.1.5-fpm as nova-image

# Установка зависимостей
RUN apt-get update && apt-get install -y \
    libmagickwand-dev \
    libmagickcore-dev \
    libzip-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libpq-dev \
    libwebp-dev \
    netcat \
    postgresql-client

# Установка расширений PHP
RUN docker-php-ext-install exif xml filter zip bcmath intl iconv pcntl pdo pdo_pgsql pgsql

# Установка библиотеки Imagick
RUN pecl install imagick && docker-php-ext-enable imagick

# Добавление конфигурации для OpenSSL
RUN echo 'openssl.enable=1' >> /usr/local/etc/php/conf.d/docker-php-ext-openssl.ini

# Установка Composer
RUN curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/local/bin

# Установка правильных разрешений для папок и файлов
WORKDIR /var/www
COPY entrypoint-php.sh /entrypoint-php.sh
COPY entrypoint-horizon.sh /entrypoint-horizon.sh
RUN chmod +x /entrypoint-php.sh
RUN chmod +x /entrypoint-horizon.sh
RUN chown -R www-data:www-data /var/www
RUN chmod -R 755 /var/www

ENTRYPOINT ["/entrypoint-php.sh"]
CMD ["php-fpm", "-F"]
